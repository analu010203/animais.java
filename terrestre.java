public class Terrestre extends Animais {

      private String alimento;

      public Terrestre (String sexo, String alimento) {
            super(sexo);//passa parâmetro para o construtor da superclasse 
            this.alimento=alimento;
      }

      public void setAlimento(String alimento) {
            this.alimento=alimento;          
      }

      public String getAlimento() {
            return alimento;        
      }
      
}

