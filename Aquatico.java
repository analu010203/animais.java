public class Aquatico extends Animais {

      private String habitat;

      public Aquatico (String sexo, String habitat) {
            super(sexo);//passa parâmetro para o construtor da superclasse 
            this.habitat=habitat;
      }

      public String setHabitat(String habitat) {
            this.habitat=habitat;          
      }

      public void getHabitat() {
            return habitat;          
      }
}